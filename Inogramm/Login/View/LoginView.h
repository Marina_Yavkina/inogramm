//
//  LoginView.h
//  Inogramm
//
//  Created by Vladislav Grigoriev on 02/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginView : UIView

@property (nonatomic, strong) UIImageView *backgroundImageView;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *instagrammLoginButton;

@end
