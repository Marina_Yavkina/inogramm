//
//  LoginView.m
//  Inogramm
//
//  Created by Vladislav Grigoriev on 02/12/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "LoginView.h"
#import <QuartzCore/QuartzCore.h>

//colors http://paletton.com/#uid=12V0u0kllllaFw0g0qFqFg0w0aF
//background http://dragdropsite.github.io/waterpipe.js/

@implementation LoginView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UIColor *textColor = [UIColor colorWithRed:16.0f / 255.0f
                                             green:94.0f / 255.0f
                                              blue:40.0f / 255.0f
                                             alpha:1.0f];
        
        UIColor *shadowColor = [UIColor colorWithRed:0.0f / 255.0f
                                               green:64.0f / 255.0f
                                                blue:19.0f / 255.0f
                                               alpha:1.0f];
        
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @".Inogramm";
        _titleLabel.font = [UIFont systemFontOfSize:64.0f];
        _titleLabel.textColor = textColor;
        _titleLabel.shadowColor = [shadowColor colorWithAlphaComponent:0.4f];
        _titleLabel.shadowOffset = CGSizeMake(1.0f, 1.0f);
        
        [self addSubview:_titleLabel];
        
        _instagrammLoginButton = [UIButton buttonWithType:UIButtonTypeSystem];
        
        [_instagrammLoginButton setTitle:@"Log in" forState:UIControlStateNormal];
        [_instagrammLoginButton setTitleColor:textColor forState:UIControlStateNormal];
        _instagrammLoginButton.contentEdgeInsets = UIEdgeInsetsMake(8.0f,
                                                                    8.0f,
                                                                    8.0f,
                                                                    8.0f);
        
        _instagrammLoginButton.titleLabel.font = [UIFont systemFontOfSize:48.0f];
        _instagrammLoginButton.layer.borderWidth = 2.0f;
        _instagrammLoginButton.layer.borderColor = textColor.CGColor;
        _instagrammLoginButton.layer.cornerRadius = 8.0f;
        
        [_instagrammLoginButton.layer setShadowColor:shadowColor.CGColor];
        [_instagrammLoginButton.layer setShadowOpacity:0.6f];
        [_instagrammLoginButton.layer setShadowRadius:0.5];
        [_instagrammLoginButton.layer setShadowOffset:CGSizeMake(1.0f, 1.0f)];
        
        [self addSubview:_instagrammLoginButton];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
        
    CGSize instagrammLoginButtonSize = [self.instagrammLoginButton sizeThatFits:self.bounds.size];
    self.instagrammLoginButton.frame = CGRectMake((self.bounds.size.width - instagrammLoginButtonSize.width) / 2.0f,
                                                  self.bounds.size.height / 2.0f + (self.bounds.size.height / 2.0f - instagrammLoginButtonSize.height) / 2.0f,
                                                  instagrammLoginButtonSize.width,
                                                  instagrammLoginButtonSize.height);
    
    CGSize titleSize = [self.titleLabel sizeThatFits:self.bounds.size];
    self.titleLabel.frame = CGRectMake((self.bounds.size.width - titleSize.width) / 2.0f,
                                       (self.bounds.size.height / 2.0f - titleSize.height) / 2.0f,
                                       titleSize.width,
                                       titleSize.height);
}

@end
